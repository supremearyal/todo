<!doctype html>
<html>
    <head>
        <style type="text/css">
            table { border-collapse: collapse; border-style: hidden; }
            table tr { border: 1px solid; }
        </style>
        <title>List of tasks to do</title>
    </head>
    <body>
        <h1>Tasks to do</h1>
        <form action="/todo" method="post">
            <table>
            %if len(tasks) > 0:
                <tr>
                    <th>Done?</th>
                    <th>Task</th>
                </tr>
                %for task in tasks:
                <tr>
                    <td>\\
                    %if task.done:
                        Yes
                        <td><del>{{task.title}}</del></td>
                    %else:
                        <a href="/done/{{task.rowid}}">No</a>
                        <td>{{task.title}}</td>
                    %end
                    </td>
                </tr>
                %end
            %else:
                <h2>No tasks yet</h2>
            %end
            </table>
            <br/>
            <input type="text" name="title"/>
            <br/>
            <input type="submit" value="Add new task"/>
        </form>
    </body>
</html>
