from bottle import Bottle, route, get, run, template, redirect, request
from bottle.ext import sqlite
import sqlite3

app = Bottle()
c = sqlite3.connect('todo.db')
c.row_factory = sqlite3.Row
c.executescript('CREATE TABLE IF NOT EXISTS todo(done INTEGER, title TEXT)')
app.install(sqlite.Plugin(dbfile='todo.db'))

class Task:
    def __init__(self, rowid, done, title):
        self.rowid = rowid
        self.done = done
        self.title = title

def get_tasks(db):
    tasks = []
    for row in db.execute('SELECT rowid, done, title FROM todo'):
        tasks.append(Task(row['rowid'], bool(row['done']), row['title']))
    return tasks

@app.get('/todo')
@app.get('/')
def todo(db):
    tasks = get_tasks(db)
    return template('todo.tpl', tasks=tasks)

@app.post('/todo')
@app.post('/')
def add_todo(db):
    title = request.forms.title
    if title:
        db.execute('INSERT INTO todo(done, title) VALUES(?, ?)', (False, title))
    redirect('/')

@app.route('/done/<rowid:int>')
def done(db, rowid):
    db.execute('UPDATE todo SET done=? WHERE rowid=?', (True, rowid))
    redirect('/')

app.run(debug=True, reloader=True)
